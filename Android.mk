LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := libcurl
LOCAL_SRC_FILES := includes/curl/libs/libcurl.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES := includes
LOCAL_SHARED_LIBRARIES := libcurl
LOCAL_MODULE    := urlenc
LOCAL_SRC_FILES := src/urlenc.c
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES := includes
LOCAL_STATIC_LIBRARIES := libcurl
LOCAL_MODULE    := urldec
LOCAL_SRC_FILES := src/urldec.c
include $(BUILD_EXECUTABLE)
