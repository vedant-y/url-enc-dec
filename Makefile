LDFLAGS := -lcurl

all: urlenc urldec

%: src/%.c
	@mkdir -p bin
	$(CC) -o bin/$@ $< $(LDFLAGS)

install: all
	mv bin/* /usr/bin/
	@rm -rf bin

build: all
	@rm -rf usr
	@mkdir -p usr
	@mv bin usr/
	@chmod -R 755 $(CURDIR)/DEBIAN
	dpkg --build $(CURDIR)

android-build:
	@# For WSL
	@cmd.exe /C ndk-build NDK_PROJECT_PATH=. NDK_APPLICATION_MK=Application.mk
	@rm -rf android-arm
	@mkdir -p android-arm
	@mv libs/armeabi-v7a/* android-arm/
	@rm -rf libs obj
	termux-create-package termux.json
	
clean:
	rm -rf usr bin android-arm libs obj

.PHONY: all install clean
